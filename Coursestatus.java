import java.util.Objects;

public class Coursestatus {

    static final Coursestatus aktivya = new Coursestatus("aktiv");
    static final Coursestatus passivya = new Coursestatus("passiv");
    public final String name;

    private Coursestatus(String name) {
        this.name = name;
    }

    public String name() {
        return name;
    }

    public static String findByName(String name) {
        if (Objects.equals(aktivya.name(), name) || Objects.equals(passivya.name(), name)) {
            return name;
        } else
            return null;
    }
}
