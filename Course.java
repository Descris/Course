public class Course {
    private Coursestatus status;

    private String name;
    private int credit;
    private String[] keywords;

    public Coursestatus getStatus() {
        return status;
    }

    public void setStatus(Coursestatus status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public String[] getKeywords() {
        return keywords;
    }

    public void setKeywords(String[] keywords) {
        this.keywords = keywords;
    }

    public Course(Coursestatus status, String name, int credit, String[] keywords) {
        this.status = status;
        this.name = name;
        this.credit = credit;
        this.keywords = keywords;
    }
}
