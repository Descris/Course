public class Student {
    private String name;
    private int age;
    private double grade;
    private Course[] kurs;

    public Student(String name, int age, double grade, Course[] kurs ){
        this.name= name;
        this.age= age;
        this.grade= grade;
        this.kurs= kurs;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public Course[] getKurs() {
        return kurs;
    }

    public void setKurs(Course[] kurs) {
        this.kurs = kurs;
    }
}